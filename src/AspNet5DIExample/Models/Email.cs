﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNet5DIExample.Models
{
    public class Email
    {
        public string EmailAddress { get; set; }
    }
}
