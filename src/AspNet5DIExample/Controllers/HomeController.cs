﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using AspNet5DIExample.Models;
using AspNet5DIExample.Services;
using Microsoft.AspNet.Mvc;

namespace AspNet5DIExample.Controllers
{
    public class HomeController : Controller
    {
        private IMyEmailSender _emailSender;
        public HomeController(IMyEmailSender emailSender)
        {
            _emailSender = emailSender;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View(new Email());
        }

        public IActionResult Error()
        {
            return View();
        }

        public async Task<ActionResult> SendEmail(Email email)
        {

            await _emailSender.SendAsyncEmail(email.EmailAddress);
            return View();
        }


    }
}
