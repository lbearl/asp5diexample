﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNet5DIExample.Services
{
    public interface IMyEmailSender
    {
        Task SendAsyncEmail(string emailAddress);
    }
}
