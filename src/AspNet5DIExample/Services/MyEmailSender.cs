﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AspNet5DIExample.Services
{
    public class MyEmailSender : IMyEmailSender
    {
        public async Task SendAsyncEmail(string emailAddress)
        {
            using (var smtp = new SmtpClient("localhost"))
            {
                var mail = new MailMessage
                {
                    Subject = "Test",
                    From = new MailAddress("test@localhost"),
                    Body = "This is a test message"
                };
                mail.To.Add(emailAddress);
                await smtp.SendMailAsync(mail);
            }
        }
    }
}
